#include <Python.h>

#include <vector>

#include <cassert>

#if defined(_WIN32)
#define LUA_EXPORT extern "C" __declspec(dllexport)
#else
#define LUA_EXPORT extern "C" __attribute__ ((visibility ("default")))
#endif

extern "C" {
#include <lua.h>
#include <lauxlib.h>
}

#if defined(_WIN32)
#include <Windows.h>
#endif

////

// MSVC doesn't support designated initializers
#if defined(_MSC_VER)
#undef _Py_static_string_init
#define _Py_static_string_init(value) { /* .next = */ NULL, /* .string = */ value, /* .object = */ NULL }
#endif
_Py_IDENTIFIER(stdout);
_Py_IDENTIFIER(stderr);
_Py_IDENTIFIER(flush);

static void flush_io(void) {
	PyObject *f, *r;
	PyObject *type, *value, *traceback;

	/* Save the current exception */
	PyErr_Fetch(&type, &value, &traceback);

	f = _PySys_GetObjectId(&PyId_stderr);
	if (f != NULL) {
		r = _PyObject_CallMethodId(f, &PyId_flush, NULL);
		if (r)
			Py_DECREF(r);
		else
			PyErr_Clear();
	}
	f = _PySys_GetObjectId(&PyId_stdout);
	if (f != NULL) {
		r = _PyObject_CallMethodId(f, &PyId_flush, NULL);
		if (r)
			Py_DECREF(r);
		else
			PyErr_Clear();
	}

	PyErr_Restore(type, value, traceback);
}

static int set_main_loader(PyObject *d, const char *filename, const char *loader_name) {
	PyInterpreterState *interp;
	PyThreadState *tstate;
	PyObject *filename_obj, *bootstrap, *loader_type = NULL, *loader;
	int result = 0;

	filename_obj = PyUnicode_DecodeFSDefault(filename);
	if (filename_obj == NULL)
		return -1;
	/* Get current thread state and interpreter pointer */
	tstate = PyThreadState_GET();
	interp = tstate->interp;
	bootstrap = PyObject_GetAttrString(interp->importlib,
			"_bootstrap_external");
	if (bootstrap != NULL) {
		loader_type = PyObject_GetAttrString(bootstrap, loader_name);
		Py_DECREF(bootstrap);
	}
	if (loader_type == NULL) {
		Py_DECREF(filename_obj);
		return -1;
	}
	loader = PyObject_CallFunction(loader_type, "sN", "__main__", filename_obj);
	Py_DECREF(loader_type);
	if (loader == NULL) {
		return -1;
	}
	if (PyDict_SetItemString(d, "__loader__", loader) < 0) {
		result = -1;
	}
	Py_DECREF(loader);
	return result;
}

static std::string to_ascii(PyObject *obj) {
	PyObject *pystr = PyObject_Str(obj);
	Py_UCS4 *msg = PyUnicode_AsUCS4Copy(pystr);
	Py_DECREF(pystr);

	if (!msg)
		return "<mem (ASCII) alloc failed>";

	size_t len = 0;
	for (Py_UCS4 *i = msg; *i; i++)
		len++;

	std::string str;
	str.reserve(len);

	for (Py_UCS4 *i = msg; *i; i++)
		str += (char) *i;

	PyMem_Free(msg);

	return str;
}

std::string formatPythonError(PyObject *exc_tb) {
	char tb_string[1024];
	std::string err_str;

	PyObject *pModule = PyImport_ImportModule("traceback");

	if (exc_tb != NULL && pModule != NULL) {
		PyObject *pDict = PyModule_GetDict(pModule);
		PyObject *pFunc = PyDict_GetItemString(pDict, "format_tb");
		if (pFunc && PyCallable_Check(pFunc)) {
			PyObject *pArgs = PyTuple_New(1);
			PyTuple_SetItem(pArgs, 0, exc_tb);
			PyObject *pValue = PyObject_CallObject(pFunc, pArgs);
			if (pValue != NULL) {
				int len = PyList_Size(pValue);
				if (len > 0) {
					PyObject *t, *tt;
					int i;
					char *buffer;
					for (i = 0; i < len; i++) {
						tt = PyList_GetItem(pValue, i);
						t = Py_BuildValue("(O)", tt);
						if (!PyArg_ParseTuple(t, "s", &buffer)) {
							return "<tuple err>";
						}

						strcpy(tb_string, buffer);
						err_str += tb_string;
						err_str += "\n";
					}
				}
			}
			Py_DECREF(pValue);
			Py_DECREF(pArgs);
		}
	}
	Py_DECREF(pModule);

	return err_str;
}

int custom_simple_file(FILE *fp, const char *filename, int closeit, PyCompilerFlags *flags) {
	PyObject *m, *d, *v;
	const char *ext;
	int set_file_name = 0, ret = -1;
	size_t len;

	m = PyImport_AddModule("__main__");
	if (m == NULL)
		return -1;
	Py_INCREF(m);
	d = PyModule_GetDict(m);
	if (PyDict_GetItemString(d, "__file__") == NULL) {
		PyObject *f;
		f = PyUnicode_DecodeFSDefault(filename);
		if (f == NULL)
			goto done;
		if (PyDict_SetItemString(d, "__file__", f) < 0) {
			Py_DECREF(f);
			goto done;
		}
		if (PyDict_SetItemString(d, "__cached__", Py_None) < 0) {
			Py_DECREF(f);
			goto done;
		}
		set_file_name = 1;
		Py_DECREF(f);
	}
	len = strlen(filename);
	ext = filename + len - (len > 4 ? 4 : 0);

	// Set __main__.__loader__
	if (set_main_loader(d, filename, "SourceFileLoader") < 0) {
		fprintf(stderr, "python: failed to set __main__.__loader__\n");
		ret = -1;
		goto done;
	}
	v = PyRun_FileExFlags(fp, filename, Py_file_input, d, d,
			closeit, flags);

	flush_io();
	if (v == NULL) {
		// Fetch the exception
		PyObject *ex_class, *ex_obj, *trace;
		PyErr_Fetch(&ex_class, &ex_obj, &trace);
		PyErr_NormalizeException(&ex_class, &ex_obj, &trace);

		// SystemExit will make Print exit the entire program - don't let that happen, and pass the return value out
		if(PyErr_ExceptionMatches(PyExc_SystemExit)) {
			PyObject *pyCode = PyObject_GetAttrString(ex_obj, "code");
			long code = PyLong_AsLong(pyCode);
			ret = (int) code;
			goto done;
		}

#if defined(_WIN32)
		std::string msg = to_ascii(ex_obj);
		std::string trace_s = to_ascii(trace);
		std::string err_s = formatPythonError(trace);
		OutputDebugStringA(msg.c_str());
		OutputDebugStringA(err_s.c_str());

		// PyErr_Restore(ex_class, ex_obj, trace);
#else
		PyErr_Print();
#endif

		goto done;
	}
	Py_DECREF(v);
	ret = 0;
done:
	if (set_file_name && PyDict_DelItemString(d, "__file__"))
		PyErr_Clear();
	Py_DECREF(m);
	return ret;
}

////

int ll_run_file(lua_State *L) {
	int arg_count = lua_gettop(L);
	wchar_t **args = new wchar_t*[arg_count];

	for(size_t i=0; i<arg_count; i++) {
		const char *utf = luaL_checkstring(L, i + 1);
		wchar_t *str = Py_DecodeLocale(utf, NULL);
		if (str == NULL) {
			luaL_error(L, "Cannot argument\n");
		}
		args[i] = str;
	}

	const char *program_utf = luaL_checkstring(L, 1);
	wchar_t *program = args[0];

	Py_SetProgramName(program); // Optional but recommended
	Py_Initialize();

	FILE *fp = fopen(program_utf, "rb");
	if(!fp) {
		luaL_error(L, "Could not open file %s", program_utf);
	}

	PySys_SetArgvEx(arg_count, args, true);

	int result = custom_simple_file(fp, program_utf, true, NULL);
	if (Py_FinalizeEx() < 0) {
		luaL_error(L, "Py_FinalizeEx failed");
	}

	for(size_t i=0; i<arg_count; i++) {
		PyMem_RawFree(args[i]);
	}

	delete[] args;

	lua_pushinteger(L, result);
	return 1;
}

LUA_EXPORT int luaopen_pyinterop(lua_State *L) {
	lua_newtable(L);

	lua_pushcfunction(L, ll_run_file);
	lua_setfield(L, -2, "run_file");

	return 1;
}
